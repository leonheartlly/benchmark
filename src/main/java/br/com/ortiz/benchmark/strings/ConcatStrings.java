package br.com.ortiz.benchmark.strings;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode( {Mode.AverageTime, Mode.Throughput} )
@OutputTimeUnit( TimeUnit.MILLISECONDS )
@Fork(value = 2, jvmArgsAppend = {"-server", "-disablesystemassertions"}, jvmArgs = {"-Xms2G", "-Xmx2G"})
@Warmup(iterations = 3)
public class ConcatStrings {

	@Param({"100","100000"})
	private int value;

	public static List<String> testValue = new ArrayList< String >(  );

	public static void main( String[] args ) throws RunnerException {

		Options opt = new OptionsBuilder()
				.include( ConcatStrings.class.getSimpleName() )
				.forks( 1 )
				.build();
		new Runner(opt).run();
	}

	@Setup
	public  void setupBenchmark(){
		for(int i = 0; i < value; i++){
			testValue.add( String.valueOf( i ) );
		}
	}

	@Benchmark
	public static void usingStringBuilder(){

		StringBuilder sb = new StringBuilder(  );
		for(String val : testValue ){
			sb.append( val );
		}
	}

	@Benchmark
	public static void usingStringConcat(){

		String concat = "";
		for(String val : testValue ){
			concat = concat.concat( val );
		}
	}

	@Benchmark
	public static void usingStringSum(){

		String sum = "";
		for(String val : testValue ){
			sum += val;
		}
	}

	@Benchmark
	public static void usingStringFormat(){

		String format = "";
		for(String val : testValue ){
			format += String.format( val );
		}
	}
}
