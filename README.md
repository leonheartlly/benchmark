Benchmark Project
================

OPS/MS = Operações por milisegundos.<br/>
MS/OP = Milisegundos por operação.

#### Concatenação de Strings

Testes práticos em Java sobre a performance de operações corriqueiras.


|Benchmark                      | Value    |Mode|  Cnt |      Score  |       Error  | Units |
| :---| :---:|:---:| :---: | :---: | :---: | :---: |
|ConcatStrings.usingStringBuilder| 100 | thrpt |   5 | 890,088 ± | 8,263|  ops/ms|
|ConcatStrings.usingStringConcat | 100| thrpt |   5 | 259,264 ± |58,694|  ops/ms|
|ConcatStrings.usingStringFormat | 100 |thrpt |  5 | 27,642 ±  |2,423 | ops/ms|
|ConcatStrings.usingStringSum    | 100|thrpt |   5 | 331,798 ±  |3,179 | ops/ms|
|ConcatStrings.usingStringBuilder |  100000 | thrpt    |5      |0,692 ± |  0,009  | ops/ms|
|ConcatStrings.usingStringConcat  |  100000 | thrpt    |5      |≈ 10⁻⁴ |               |ops/ms|
|ConcatStrings.usingStringFormat  |   100000 |thrpt    |5      |≈ 10⁻⁴ |               |ops/ms|
|ConcatStrings.usingStringSum     |  100000 | thrpt    |5      |≈ 10⁻⁴ |               |ops/ms|
|ConcatStrings.usingStringBuilder  |  100 |  avgt    |   5     |0,001 ± |      0,001         |ms/op|
|ConcatStrings.usingStringConcat   |   100 | avgt    |   5    |0,004 ± |       0,001        |ms/op|
|ConcatStrings.usingStringFormat   |  100 |  avgt    |   5     |0,035 ± |       0,001        |ms/op|
|ConcatStrings.usingStringSum      |  100 |  avgt    |   5     |0,003 ±|         0,001      |ms/op|
|ConcatStrings.usingStringBuilder  |  100000 |  avgt    |   5     |1,446 ±|      0,017         |ms/op|
|ConcatStrings.usingStringConcat   |   100000 | avgt    |   5    |6099,257 ± |     482,585     |ms/op|
|ConcatStrings.usingStringFormat   |  100000 |  avgt    |   5     |20293,841 ± |    59,738     |ms/op|
|ConcatStrings.usingStringSum      |  100000 |  avgt    |   5     |7882,202 ± |     148,224    |ms/op|